import 'package:flutter/material.dart';
import 'package:mail_app/model/mail.dart';

class ContentWidget extends StatelessWidget {
  final Mail mail;

  ContentWidget(this.mail);

  @override
  Widget build(BuildContext context) {
    // TODO: implementar compilación

    return Scaffold(
      backgroundColor: Colors.teal, // Color fondo
      appBar: AppBar(
        centerTitle: true, // Center title
        title: Text(mail.subject),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text('From',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 17.0,
                      fontStyle: FontStyle.italic,
                      color: Colors.brown)),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(mail.from,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 17.0,
                      fontStyle: FontStyle.italic,
                      color: Colors.white)),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(mail.subject,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 17.0,
                      fontStyle: FontStyle.italic,
                      color: Colors.black)),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Text(mail.dateTime,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 17.0,
                      fontStyle: FontStyle.italic,
                      color: Colors.brown)),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                mail.body,
                textAlign: TextAlign.justify,
                style: TextStyle(fontSize: 18.0, color: Colors.black),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
