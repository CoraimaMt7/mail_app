import 'package:flutter/material.dart';
import 'package:mail_app/model/mail.dart';
import 'package:mail_app/widgets/content_widget.dart';
import 'package:mail_app/model/Back.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

//crear widget
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, //Quitar debug de la app
      title: "Mail App",
      theme: ThemeData(
        primarySwatch: Colors.pink,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.teal,
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Correo electronico'),
        ),
        body: ListView.builder(
            itemCount: Backend().getMails().length,
            itemBuilder: (context, index) {
              Mail mail = Backend().getMails()[index];
              return Dismissible(
                key: ObjectKey(mail),
                child: ListTile(
                  subtitle: Text(mail.subject),
                  title: Text(mail.from),
                  leading: Icon(Icons.brightness_1, color: Colors.pink),
                  trailing: Text(mail.dateTime),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ContentWidget(mail)));
                  },
                ),
                onDismissed: (direction) {
                  Backend().getMails().remove(index);
                },
              );
            }));
  }
}
